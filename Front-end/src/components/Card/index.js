/* eslint-disable react/destructuring-assignment */
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import { Container, Title, Description } from './styles';

const Card = props => (
  <Container>
    <Fragment>
      <img src={props.src} alt="Imagem do Meetup" />
    </Fragment>
    <Description>
      <div>
        <Title>{props.title}</Title>
        <small>{props.members}</small>
      </div>
      <Link className="link" to={props.to}>
        <FontAwesomeIcon icon={faChevronRight} />
      </Link>
    </Description>
  </Container>
);

Card.propTypes = {
  src: PropTypes.string,
  title: PropTypes.string.isRequired,
  members: PropTypes.string,
  to: PropTypes.string.isRequired,
};

Card.defaultProps = {
  src: '',
  members: '120 members',
};

export default Card;
