import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  color: #000;
  margin: 20px 10px;
  width: 280px;

  img {
    border-radius: 5px 5px 0 0;
    max-width: 100%;
  }
`;

export const Title = styled.strong`
  color: #000;
  display: block;
  font-size: 16;
`;

export const Description = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fff;
  border-radius: 0 0 5px 5px;
  padding: 25px 15px;
  height: 90px;
  width: 100%;

  small {
    color: #999;
  }

  .link {
    background-color: #e5556e;
    border: 0;
    border-radius: 20px;
    color: #fff;
    padding: 10px 14px;
  }
`;
