import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { Container, Input, Button } from './styles';

const Search = props => (
  <Container className="search">
    <Button>
      <FontAwesomeIcon icon={faSearch} />
    </Button>
    <Input placeholder={props.placeholder} onChange={props.onChange} />
  </Container>
);

Search.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

Search.defaultProps = {
  placeholder: 'Buscar meetups',
};

export default Search;
