import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  margin-bottom: 20px;
`;

export const Input = styled.input`
  background-color: #2d3342;
  border: 0;
  border-radius: 0 4px 4px 0;
  color: #8e8e93;
  flex: 1;
  font-weight: normal;
  padding: 10px;
  width: 100%;
`;

export const Button = styled.button`
  background-color: #2d3342;
  color: #8e8e93;
  border: 0;
  border-radius: 4px 0 0 4px;
  padding: 10px;
`;
