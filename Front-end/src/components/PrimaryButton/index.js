import React from 'react';
import { Button } from './styles';

const PrimaryButton = props => <Button onClick={props.onClick}>{props.children}</Button>;

export default PrimaryButton;
