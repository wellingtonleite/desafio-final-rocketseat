import styled from 'styled-components';

export const Button = styled.button`
  background-color: #e5556e;
  border: 0;
  border-radius: 50px;
  color: #fff;
  font-size: 16px;
  padding: 10px 0;
  text-align: center;
  max-width: 100%;
`;
