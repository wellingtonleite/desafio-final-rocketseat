import styled from 'styled-components';

export const Button = styled.button`
  background-color: #e5556e;
  border: 0;
  border-radius: 20px;
  color: #fff;
  padding: 10px 14px;
`;
