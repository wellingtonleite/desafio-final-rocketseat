import React from 'react';
import { Button } from './styles';

const SecondaryButton = props => <Button onClick={() => props.onClick}>{props.children}</Button>;

export default SecondaryButton;
