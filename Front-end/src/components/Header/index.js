/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { logout } from '../../services/authService';
import { Head, Button } from './styles';
import logoWhite from '../../assets/logo-white.svg';

class Header extends Component {
  state = {
    dropdown: false,
  };

  handleLogout = async () => {
    const { history } = this.props;
    try {
      await logout();
      history.push('/');
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { onClick } = this.props;
    const { dropdown } = this.state;
    return (
      <Head>
        <ul>
          <li>
            <img src={logoWhite} alt="Logotipo Branca" />
          </li>
          <li>
            <Link to="/index">Início</Link>
          </li>
          <li>
            <Button type="button" onClick={onClick}>
              Buscar
            </Button>
          </li>
          <li>
            <Link to="meetup/new">Novo meetup</Link>
          </li>
        </ul>
        <button type="button" onClick={() => this.setState({ dropdown: !dropdown })}>
          <FontAwesomeIcon icon={faUser} />
        </button>
        {dropdown && (
          <ul className="dropdown">
            <li>
              <Link to="/profile">Editar</Link>
            </li>
            <li>
              <button type="button" onClick={this.handleLogout}>
                Sair
              </button>
            </li>
          </ul>
        )}
      </Head>
    );
  }
}

export default withRouter(Header);
