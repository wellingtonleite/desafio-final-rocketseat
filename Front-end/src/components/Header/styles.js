import styled from 'styled-components';

export const Head = styled.header`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  background-color: #e5556e;
  padding: 20px;

  ul {
    display: flex;
    justify-content: center;
    margin-left: 10px;
  }

  li {
    margin-left: 20px;
  }

  li a {
    font-size: 15px;
  }

  button {
    background-color: transparent;
    border: 0;
    color: #fff;
  }

  .dropdown {
    display: flex;
    align-items: center;
    flex-direction: column;
    position: absolute;
    right: 0px;
    top: 65px;
    background: #2d3342;
    width: 125px;
    height: 100px;

    li button,
    li a {
      font-size: 16px;
      margin: 10px;
    }
  }
`;

export const Button = styled.button`
  background-color: transparent;
  border: 0;
  color: #fff;
  font-size: 15px;
  padding: 0;
`;
