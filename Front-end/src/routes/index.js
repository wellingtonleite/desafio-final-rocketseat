import React from 'react';
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom';
import { isAuthenticated } from '../services/authService';

import Signup from '../pages/Signup';
import Signin from '../pages/Signin';
import Dashboard from '../pages/Dashboard';
import Profile from '../pages/Profile';
import MeetupDescription from '../pages/MeetupDescription';
import MeetupNew from '../pages/MeetupNew';
import Preferences from '../pages/Preferences';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuthenticated() ? (
      <Component {...props} />
    ) : (
      <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    ))
    }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Signin} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/preferences" component={Preferences} />
      <PrivateRoute exact path="/index" component={Dashboard} />
      <PrivateRoute exact path="/profile" component={Profile} />
      <PrivateRoute exact path="/meetup-description/:id" component={MeetupDescription} />
      <PrivateRoute exact path="/meetup/new" component={MeetupNew} />
      <Route path="*" component={() => <h1>Page not found</h1>} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
