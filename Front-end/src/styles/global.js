import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: Helvetica-Bold, sans-serif;
    font-weight: bold;
    outline: 0;
  }

  body{
    background-color: #1D2331;
    color:#ffffff;
  }

  li{
    list-style: none;
  }

  a{
    text-decoration: none;
    color: inherit;
    font-size: 12px;
  }

  button{
    cursor: pointer;
  }

  .error{
    color: #e5556e;
    text-align: center;
    margin: 10px 0;
  }

  .warning{
    color: #999;
    font-size: 16px;
    margin: 10px auto;
    text-align: center;
  }

`;
