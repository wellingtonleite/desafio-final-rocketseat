/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form } from './styles';
import logo from '../../assets/logo.svg';
import PrimaryButton from '../../components/PrimaryButton';
import api from '../../services/api';
import { login } from '../../services/authService';

class Signin extends Component {
  state = {
    email: '',
    password: '',
    error: '',
  };

  handleSignin = async (e) => {
    const { email, password } = this.state;
    const { history } = this.props;
    e.preventDefault();

    if (!email || !password) {
      this.setState({ error: 'Preencha e-mail e senha!' });
    } else {
      try {
        const response = await api.post('/signin', { email, password });
        login(response.data.token);
        history.push('/index');
      } catch (err) {
        console.log('Erro no login', err);
        this.setState({
          error: 'Ocorreu algum problema, verifique suas credências!',
        });
      }
    }
  };

  render() {
    const { error } = this.state;
    return (
      <Container>
        <img src={logo} alt="Logotipo" />
        {error && <p className="error">{error}</p>}
        <Form onSubmit={this.handleSignin}>
          <label htmlFor="email">E-mail</label>
          <input
            id="email"
            type="email"
            name="email"
            placeholder="Digite seu e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <label htmlFor="senha">Senha</label>
          <input
            id="senha"
            type="password"
            name="password"
            placeholder="Sua senha secreta"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <PrimaryButton>Entrar</PrimaryButton>
        </Form>
        <Link to="/signup">Criar conta grátis </Link>
      </Container>
    );
  }
}
export default withRouter(Signin);
