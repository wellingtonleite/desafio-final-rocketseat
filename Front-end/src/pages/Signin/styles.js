import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding-top: 160px;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 20px auto;
  max-width: 300px;
  width: 100%;

  label {
    font-size: 19px;
  }

  input {
    flex: 1;
    background-color: #1d2331;
    border: 0;
    color: rgba(255, 255, 255, 0.5);
    margin-bottom: 20px;
    height: 24px;
  }
`;
