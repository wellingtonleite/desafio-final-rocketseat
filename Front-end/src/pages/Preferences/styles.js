import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding-top: 160px;

  p {
    font-weight: normal;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 300px;
  width: 100%;

  h2 {
    margin-bottom: 20px;
  }
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 20px auto;
  max-width: 300px;
  width: 100%;

  label {
    font-size: 19px;
  }

  input {
    flex: 1;
    background-color: #1d2331;
    border: 0;
    color: rgba(255, 255, 255, 0.5);
    margin-bottom: 20px;
    height: 24px;
  }

  label[for='imagem'] {
    border: 2px dashed #999;
    color: #999;
    display: flex;
    justify-content: center;
    margin: 10px 0;
    padding: 40px;
  }

  input#imagem {
    display: none;
  }

  ul {
    margin-top: 10px;
  }

  ul li {
    font-weight: normal;
    margin: 5px 0;
  }

  input[type='checkbox'] {
    border: 0;
    display: inline-block;
    margin-right: 5px;
    height: 16px;
    vertical-align: top;
    border-radius: 4px;
  }

  input[type='checkbox']:checked {
    background: #a0a0a0;
  }
`;
