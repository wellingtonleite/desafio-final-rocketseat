/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import { Container, Content, Form } from './styles';
import api from '../../services/api';
import PrimaryButton from '../../components/PrimaryButton';

export default class Preferences extends Component {
  state = {
    preferences: [],
    user: {},
    themes: [{}],
    error: null,
  };

  componentDidMount() {
    this.getPreferences();
    this.getUser();
  }

  getPreferences = async () => {
    try {
      const response = await api.get('/preferences');
      this.setState({
        themes: response.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  getUser = async () => {
    try {
      const response = await api.get('/user');
      this.setState({
        user: response.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  handlePreferences = async (e) => {
    e.preventDefault();

    const { preferences } = this.state;
    const { history } = this.props;

    if (!preferences.length) {
      this.setState({
        error: 'Você precisa selecionar suas preferências!',
      });
    } else {
      const response = await api.post('/preferences', { preferences });
      console.log(response, preferences);
      history.push('/index');
    }
  };

  handleChange = (param) => {
    const value = parseInt(param);
    const { preferences } = this.state;

    const find = preferences.find(preference => preference === value);

    if (find === value) {
      return preferences.filter(preference => preference !== value);
    }
    this.setState({
      preferences: [...preferences, value],
    });
  };

  render() {
    const { themes, user, error } = this.state;
    return (
      <Container>
        <Content>
          <h2>
            Olá,
            {' '}
            <span>{user.name}</span>
          </h2>
          <p>
            Parece que é seu primeiro acesso por aqui, comece escolhendo algumas preferências para
            selecionarmos os melhores meetups pra você:
          </p>
          {error && <p className="error">{error}</p>}
          <Form onSubmit={this.handlePreferences}>
            <label htmlFor="preferences">Preferências</label>
            <ul>
              {themes.map(theme => (
                <li key={theme.id}>
                  <input
                    id="preferences"
                    type="checkbox"
                    name="preferences"
                    placeholder="Onde seu meetup irá acontecer?"
                    value={theme.id}
                    onChange={e => this.handleChange(e.target.value)}
                  />
                  {theme.name}
                </li>
              ))}
            </ul>
            <PrimaryButton type="submit">Salvar</PrimaryButton>
          </Form>
        </Content>
      </Container>
    );
  }
}
