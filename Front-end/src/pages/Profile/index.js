/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component, Fragment } from 'react';
import api from '../../services/api';
import PrimaryButton from '../../components/PrimaryButton';
import Header from '../../components/Header';
import { Container, Form } from './styles';

class Profile extends Component {
  state = {
    name: null,
    password: null,
    preferences: [],
    loading: false,
    themes: [{}],
  };

  componentDidMount() {
    this.getPreferences();
    this.getProfile();
  }

  handleProfile = async (e) => {
    e.preventDefault();
    const { name, password, preferences } = this.state;

    if (password) {
      try {
        await api.post('/user', { name, password, preferences });
      } catch (err) {
        console.log(err);
      }
    } else {
      try {
        await api.post('/user', { name, preferences });
      } catch (err) {
        console.log(err);
      }
    }
  };

  getProfile = async () => {
    const response = await api.get('/user');
    try {
      const { name, preferences } = response.data;
      this.setState({
        name,
        preferences,
      });
    } catch (err) {
      console.log(err);
    }
  };

  getPreferences = async () => {
    const response = await api.get('/preferences');

    try {
      this.setState({
        themes: response.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  handleChange = (param) => {
    const value = parseInt(param);
    const { preferences } = this.state;

    const find = preferences.find(preference => preference === value);

    if (find === value) {
      return preferences.filter(preference => preference !== value);
    }
    this.setState({
      preferences: [...preferences, value],
    });
  };

  verifyPreferences = (id) => {
    const { preferences } = this.state;

    console.log(id);

    if (id !== undefined) {
      this.setState({
        loading: true,
      });
    }

    const find = preferences.find(preference => preference === id);

    if (find === id) {
      return (
        <input
          id="theme"
          type="checkbox"
          name="preferences"
          placeholder="Onde seu meetup irá acontecer?"
          value={id}
          checked="checked"
          onChange={e => this.handleChange(e.target.value)}
        />
      );
    }
    return (
      <input
        id="theme"
        type="checkbox"
        name="preferences"
        placeholder="Onde seu meetup irá acontecer?"
        value={id}
        onChange={e => this.handleChange(e.target.value)}
      />
    );
  };

  render() {
    const { name, password, themes } = this.state;
    return (
      <Fragment>
        <Header />
        <Container>
          <Form>
            <label htmlFor="name">Nome</label>
            <input
              id="name"
              name="name"
              value={name}
              onChange={e => this.setState({ name: e.target.value })}
            />
            <label htmlFor="password">Senha</label>
            <input
              id="password"
              type="password"
              name="password"
              value={password}
              onChange={e => this.setState({ password: e.target.value })}
            />
            <label htmlFor="theme">Preferências</label>
            <ul>
              {themes.map(theme => (
                <li key={theme.id}>
                  <Fragment>
                    {/* {loading && this.verifyPreferences(theme.id)} */}
                    <input
                      id="theme"
                      type="checkbox"
                      name="preferences"
                      placeholder="Onde seu meetup irá acontecer?"
                      value={theme.id}
                      onChange={e => this.handleChange(e.target.value)}
                    />
                    {theme.name}
                  </Fragment>
                </li>
              ))}
            </ul>
            <PrimaryButton type="submit">Salvar</PrimaryButton>
          </Form>
        </Container>
      </Fragment>
    );
  }
}

export default Profile;
