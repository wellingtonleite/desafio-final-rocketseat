import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin-top: 20px;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 900px;
  width: 100%;

  h2 {
    align-self: flex-start;
    font-size: 16px;
    margin: 10px;
  }
`;

export const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
