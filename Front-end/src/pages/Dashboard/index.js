import React, { Component, Fragment } from 'react';
import Header from '../../components/Header';
import Card from '../../components/Card';
import api from '../../services/api';
import { Container, Content, Cards } from './styles';
import Search from '../../components/Search';

class Dashboard extends Component {
  state = {
    display: false,
    searchInput: null,
    meetups: [{}],
    subMeetups: [{}],
    nextMeetups: [{}],
    recMeetups: [{}],
  };

  componentDidMount() {
    this.getSubMeetups();
    this.getNextMeetups();
    this.getRecMeetups();
  }

  getSubMeetups = async () => {
    try {
      const response = await api.get('/registered');
      this.setState({
        subMeetups: response.data.meetups,
      });
    } catch (err) {
      console.log(err);
    }
  };

  getNextMeetups = async () => {
    try {
      const response = await api.get('/next-meetups');
      this.setState({
        nextMeetups: response.data.meetups,
      });
    } catch (err) {
      console.log(err);
    }
  };

  getRecMeetups = async () => {
    try {
      const response = await api.get('/meetups');
      this.setState({
        recMeetups: response.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  showInput = () => {
    const { display } = this.state;
    this.setState({
      display: !display,
    });
  };

  handleSearch = async (param) => {
    const { searchInput } = this.state;

    this.setState({
      searchInput: param,
    });

    const response = await api.get(`meetups?title=${searchInput}`);
    console.log(response);
    this.setState({
      meetups: response.data,
    });
  };

  render() {
    const {
      meetups, subMeetups, nextMeetups, recMeetups, display, searchInput,
    } = this.state;
    return (
      <Fragment>
        <Header onClick={this.showInput} />
        <Container>
          <Content>
            {display ? <Search onChange={e => this.handleSearch(e.target.value)} /> : null}
            {searchInput ? (
              <Cards>
                {meetups.map(meetup => (
                  <Card
                    key={meetup.id}
                    members={meetup.members}
                    title={meetup.title}
                    to={`/meetup-description/${meetup.id}`}
                  />
                ))}
              </Cards>
            ) : (
              <Fragment>
                {!subMeetups.length ? (
                  <p className="warning">
                    Você não está inscrito em nenhum meetup, você pode dar uma olhada nos
                    recomendados e se inscrever!
                  </p>
                ) : (
                  <Fragment>
                    <h2>Inscrições</h2>
                    <Cards>
                      {subMeetups.map(meetup => (
                        <Card
                          key={meetup.id}
                          members={meetup.members}
                          title={meetup.title}
                          to={`/meetup-description/${meetup.id}`}
                        />
                      ))}
                    </Cards>
                    <h2>Próximos meetups</h2>
                    <Cards>
                      {nextMeetups.map(nextMeetup => (
                        <Card
                          key={nextMeetup.id}
                          members={nextMeetup.members}
                          title={nextMeetup.title}
                          to={`/meetup-description/${nextMeetup.id}`}
                        />
                      ))}
                    </Cards>
                  </Fragment>
                )}
                <h2>Recomendados</h2>
                <Cards>
                  {recMeetups.map(meetup => (
                    <Card
                      key={meetup.id}
                      members={meetup.members}
                      title={meetup.title}
                      to={`/meetup-description/${meetup.id}`}
                    />
                  ))}
                </Cards>
              </Fragment>
            )}
          </Content>
        </Container>
      </Fragment>
    );
  }
}

export default Dashboard;
