import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 40px;
  max-width: 900px;

  img {
    max-width: 100%;
  }
`;

export const Description = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 300px;
  width: 100%;

  h2 {
    margin: 20px 0;
  }

  p {
    font-weight: normal;
    margin-bottom: 20px;
  }

  small {
    color: #999;
    margin-bottom: 5px;
  }
`;
