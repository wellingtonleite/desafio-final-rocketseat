/* eslint-disable react/prop-types */
import React, { Component, Fragment } from 'react';
import Header from '../../components/Header';
import PrimaryButton from '../../components/PrimaryButton';
import { Container, Content, Description } from './styles';
import api from '../../services/api';

class MeetupDescription extends Component {
  state = {
    meetup: {},
  };

  componentDidMount() {
    this.getMeetup();
  }

  getMeetup = async () => {
    const {
      match: { params },
    } = this.props;
    const { id } = params;

    try {
      const response = await api.get(`/meetup/${id}`);
      this.setState({
        meetup: response.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  handleSubscribe = async () => {
    const {
      match: { params },
    } = this.props;
    const { id } = params;
    const { history } = this.props;
    try {
      const response = await api.post(`/subscribe/${id}`);
      history.push('/index');
      console.log(response);
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { meetup } = this.state;
    return (
      <Fragment>
        <Header />
        <Container>
          <Content>
            <figure>
              <img src={meetup.image} alt="Imagem do meetup" />
            </figure>
            <Description>
              <h2>{meetup.title}</h2>
              <p>{meetup.description}</p>
              <small>Realizado em:</small>
              <p>{meetup.location}</p>
              <small>Quando?</small>
              <p>{meetup.date}</p>
              <PrimaryButton onClick={this.handleSubscribe}>Inscreva-se</PrimaryButton>
            </Description>
          </Content>
        </Container>
      </Fragment>
    );
  }
}

export default MeetupDescription;
