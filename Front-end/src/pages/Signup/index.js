/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form } from './styles';
import logo from '../../assets/logo.svg';
import api from '../../services/api';
import { login } from '../../services/authService';
import PrimaryButton from '../../components/PrimaryButton';

class Signin extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    error: '',
  };

  handleSignup = async (e) => {
    const { history } = this.props;
    const { name, email, password } = this.state;
    e.preventDefault();

    if (!name || !email || !password) {
      this.setState({ error: 'Você precisa preencher todos os campos!' });
    } else {
      try {
        const response = await api.post('/signup', { name, email, password });
        login(response.data.token);
        history.push('/preferences');
      } catch (err) {
        this.setState({ error: 'Ocorreu algum problema com o seu cadastro' });
      }
    }
  };

  render() {
    const { error } = this.state;
    return (
      <Container>
        <img src={logo} alt="Logotipo" />
        {error && <p className="error">{error}</p>}
        <Form onSubmit={this.handleSignup}>
          <label htmlFor="name">Nome</label>
          <input
            id="name"
            type="text"
            name="name"
            placeholder="Digite seu nome"
            onChange={e => this.setState({ name: e.target.value })}
          />
          <label htmlFor="email">E-mail</label>
          <input
            id="email"
            type="email"
            name="email"
            placeholder="Digite seu e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <label htmlFor="senha">Senha</label>
          <input
            id="senha"
            type="password"
            name="password"
            placeholder="Sua senha secreta"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <PrimaryButton>Criar conta</PrimaryButton>
        </Form>
        <Link to="/">Já tenho conta</Link>
      </Container>
    );
  }
}

export default withRouter(Signin);
