/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component, Fragment } from 'react';
import { faCamera } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Header from '../../components/Header';
import { Container, Form } from './styles';
import PrimaryButton from '../../components/PrimaryButton';
import api from '../../services/api';

class MeetupNew extends Component {
  state = {
    title: null,
    description: null,
    date: null,
    image: null,
    location: null,
    preferences: [],

    themes: [{}],
  };

  componentDidMount() {
    this.getPreferences();
  }

  getPreferences = async () => {
    const response = await api.get('preferences');

    this.setState({
      themes: response.data,
    });
  };

  handleMeetup = async (e) => {
    e.preventDefault();

    const { history } = this.props;
    const {
      title, description, date, image, location, preferences,
    } = this.state;

    if (image) {
      const data = new FormData();
      data.append('image', image, image.name);

      const config = {
        headers: {
          'content-type': 'multipart/form-data',
        },
      };

      try {
        const response = await api.post('meetup', {
          title,
          description,
          date,
          image: data,
          location,
          preferences,
          config,
        });
        console.log(response);
        history.push('/index');
      } catch (err) {
        console.log(err);
      }
    }

    try {
      const response = await api.post('meetup', {
        title,
        description,
        date,
        location,
        preferences,
      });
      console.log(response);
      history.push('/index');
    } catch (err) {
      console.log(err);
    }
  };

  handleChange = (param) => {
    const value = parseInt(param);
    const { preferences } = this.state;

    const find = preferences.find(preference => preference === value);

    if (find === value) {
      return preferences.filter(preference => preference !== value);
    }
    this.setState({
      preferences: [...preferences, value],
    });
  };

  handleFile = (e) => {
    const image = e.target.files[0];

    this.setState({
      image,
    });
  };

  render() {
    const { themes } = this.state;
    return (
      <Fragment>
        <Header />
        <Container>
          <Form onSubmit={this.handleMeetup}>
            <label htmlFor="title">Título</label>
            <input
              id="title"
              type="text"
              name="title"
              placeholder="Digite o título do Meetup"
              onChange={e => this.setState({ title: e.target.value })}
            />
            <label htmlFor="description">Descrição</label>
            <textarea
              id="description"
              name="description"
              placeholder="Descreva seu meetup"
              onChange={e => this.setState({ description: e.target.value })}
            />
            <label htmlFor="data">Data/Hora</label>
            <input
              id="data"
              name="date"
              placeholder="Quando o meetup vai acontecer?"
              onChange={e => this.setState({ date: e.target.value })}
            />
            <label>Imagem</label>
            <label htmlFor="imagem">
              <FontAwesomeIcon icon={faCamera} />
            </label>
            <input id="imagem" type="file" name="image" onChange={e => this.handleFile(e)} />
            <label htmlFor="location">Localização</label>
            <input
              id="location"
              type="text"
              name="location"
              placeholder="Onde seu meetup irá acontecer?"
              onChange={e => this.setState({ location: e.target.value })}
            />
            <label htmlFor="theme">Tema do meetup</label>
            <ul>
              {themes.map(theme => (
                <li key={theme.id}>
                  <Fragment>
                    <input
                      id="theme"
                      type="checkbox"
                      name="preferences"
                      placeholder="Onde seu meetup irá acontecer?"
                      value={theme.id}
                      onChange={e => this.handleChange(e.target.value)}
                    />
                    {theme.name}
                  </Fragment>
                </li>
              ))}
            </ul>
            <PrimaryButton type="submit">Salvar</PrimaryButton>
          </Form>
        </Container>
      </Fragment>
    );
  }
}

export default MeetupNew;
