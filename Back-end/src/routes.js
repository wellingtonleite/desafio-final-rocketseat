const express = require('express')
const cors = require('cors')
const multerConfig = require('./config/multer')
const upload = require('multer')(multerConfig)

const routes = express.Router()

const controllers = require('./app/controllers')
const UserController = require('./app/controllers/UserController')

routes.use(cors())

routes.post('/signup', controllers.UserController.store)
routes.post('/signin', controllers.SessionController.store)

const authMiddleware = require('./app/middlewares/auth')

const PreferenceController = require('./app/controllers/PreferenceController')
const UserPreferenceController = require('./app/controllers/UserPreferenceController')
const MeetupController = require('./app/controllers/MeetupController')
const SubscribeController = require('./app/controllers/SubscribeController')

routes.use(authMiddleware)

routes.get('/user', UserController.index)
routes.put('/user', upload.single('avatar'), UserController.update)

routes.get('/preferences', PreferenceController.index)
routes.post('/preferences', UserPreferenceController.store)

routes.get('/meetups', MeetupController.index)
routes.get('/meetup/:id', MeetupController.show)
routes.put('/meetup/:id', MeetupController.update)
routes.post('/meetup', upload.single('image'), MeetupController.store)
routes.delete('/meetup/:id', MeetupController.destroy)

routes.get('/registered', SubscribeController.index)
routes.get('/next-meetups', SubscribeController.show)
routes.post('/subscribe/:id', SubscribeController.store)

module.exports = routes
