const { Meetup, User } = require('../models')
const moment = require('moment')
const { Op } = require('sequelize')

class SubscribeController {
  async index (req, res) {
    const meetups = await User.findByPk(req.userId, {
      include: [{ model: Meetup, as: 'meetups' }]
    })

    return res.json(meetups)
  }

  async show (req, res) {
    // Mostra apenas os Meetups do mês (Ou seja os mais próximos)
    const meetups = await User.findByPk(req.userId, {
      include: [{ model: Meetup, as: 'meetups' }],
      where: {
        date: {
          [Op.between]: [
            moment()
              .startOf('day')
              .format(),
            moment()
              .endOf('month')
              .format()
          ]
        }
      }
    })

    return res.json(meetups)
  }

  async store (req, res) {
    const { id } = req.params

    const user = await User.findByPk(req.userId)

    user.addMeetups(id)

    return res.json(user)
  }
}

module.exports = new SubscribeController()
