const { User, Preference } = require('../models')

class UserPreferenceController {
  async store (req, res) {
    const { preferences } = req.body

    const user = await User.findByPk(req.userId, {
      include: [
        { model: Preference, as: 'preferences', through: { attributes: [] } }
      ]
    })

    user.addPreferences(preferences)

    return res.json(user)
  }
}

module.exports = new UserPreferenceController()
