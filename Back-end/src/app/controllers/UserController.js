const { User, Preference } = require('../models')

class UserController {
  async index (req, res) {
    const user = await User.findByPk(req.userId)

    return res.json(user)
  }

  async store (req, res) {
    const { email } = req.body

    if (await User.findOne({ where: { email } })) {
      return res.status(400).json({ error: 'User already exist' })
    }

    const user = await User.create(req.body)

    return res.json({ user, token: user.generateToken(user) })
  }

  async update (req, res) {
    const { filename: avatar } = req.file
    const { preferences, ...data } = req.body

    const user = await User.findByPk(req.userId, {
      include: [
        { model: Preference, as: 'preferences', through: { attributes: [] } }
      ]
    })

    user.setPreferences(preferences)

    await User.update(data, avatar, { where: { id: user.id } })

    return res.json(user)
  }
}

module.exports = new UserController()
