const { Preference, Meetup, User } = require('../models')
const { Op } = require('sequelize')

class MeetupController {
  async index (req, res) {
    const { title } = req.query

    if (title) {
      const meetups = await Meetup.findAll({
        include: [
          { model: Preference, as: 'preferences', through: { attributes: [] } },
          {
            model: User,
            as: 'subscribe',
            include: [
              {
                model: Preference,
                as: 'preferences',
                through: { attributes: [] }
              }
            ]
          }
        ],
        where: {
          title: {
            [Op.like]: `%${title}%`
          }
        }
      })

      return res.json(meetups)
    }

    const meetups = await Meetup.findAll({
      include: [
        { model: Preference, as: 'preferences', through: { attributes: [] } },
        {
          model: User,
          as: 'subscribe',
          include: [
            {
              model: Preference,
              as: 'preferences',
              through: { attributes: [] }
            }
          ]
        }
      ]
    })

    return res.json(meetups)
  }

  async show (req, res) {
    const { id } = req.params

    const meetup = await Meetup.findByPk(id)

    return res.json(meetup)
  }

  async store (req, res) {
    const { preferences, ...data } = req.body

    if (req.file) {
      const { filename: image } = req.file
      const meetup = await Meetup.create({ ...data, userId: req.userId, image })

      meetup.addPreferences(preferences)

      return res.json(meetup)
    } else {
      const meetup = await Meetup.create({ ...data, userId: req.userId })

      meetup.addPreferences(preferences)

      return res.json(meetup)
    }
  }

  async update (req, res) {
    const { id } = req.params
    const { preferences, ...data } = req.body

    const meetup = await Meetup.findByPk(id)

    meetup.update(data).then(function (response) {
      meetup.setPreferences(preferences)
    })

    return res.json(meetup)
  }

  async destroy (req, res) {
    const { id } = req.params

    Meetup.destroy({ where: { id } })

    return res.send()
  }
}

module.exports = new MeetupController()
