const { Preference } = require('../models')

class PreferenceController {
  async index (req, res) {
    const preferences = await Preference.findAll()

    return res.json(preferences)
  }
}

module.exports = new PreferenceController()
