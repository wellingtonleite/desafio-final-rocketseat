module.exports = (sequelize, DataTypes) => {
  const MeetupSubscribe = sequelize.define('MeetupSubscribe', {
    user_id: DataTypes.INTEGER,
    meetup_id: DataTypes.INTEGER
  })

  return MeetupSubscribe
}
