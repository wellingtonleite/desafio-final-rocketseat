module.exports = (sequelize, DataTypes) => {
  const Preference = sequelize.define('Preference', {
    name: DataTypes.STRING
  })

  Preference.associate = models => {
    Preference.belongsToMany(models.User, {
      through: 'users_preferences',
      as: 'user',
      foreingKey: 'preference_id'
    })

    Preference.belongsToMany(models.Meetup, {
      through: 'meetups_preferences',
      as: 'meetup',
      foreingKey: 'preference_id'
    })
  }

  return Preference
}
