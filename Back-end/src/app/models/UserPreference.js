module.exports = (sequelize, DataTypes) => {
  const UserPreference = sequelize.define('UserPreference', {
    user_id: DataTypes.INTEGER,
    preference_id: DataTypes.INTEGER
  })

  return UserPreference
}
