module.exports = (sequelize, DataTypes) => {
  const Meetup = sequelize.define('Meetup', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    image: DataTypes.STRING,
    location: DataTypes.STRING,
    date: DataTypes.DATE
  })

  Meetup.associate = models => {
    Meetup.belongsToMany(models.Preference, {
      through: 'meetups_preferences',
      as: { singular: 'preference', plural: 'preferences' },
      foreingKey: 'meetup_id'
    })

    Meetup.belongsToMany(models.User, {
      through: 'meetups_subscribes',
      as: 'subscribe',
      foreingKey: 'meetup_id'
    })

    Meetup.belongsTo(models.User, {
      as: 'user',
      foreingKey: 'user_id'
    })
  }
  return Meetup
}
