const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const authConfig = require('../../config/Auth')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      avatar: DataTypes.STRING,
      password: DataTypes.STRING
    },
    {
      hooks: {
        beforeSave: async user => {
          if (user.password) {
            user.password = await bcrypt.hash(user.password, 8)
          }
        }
      }
    }
  )

  User.prototype.checkPassword = function (password) {
    return bcrypt.compare(password, this.password)
  }

  User.prototype.generateToken = function ({ id }) {
    return jwt.sign({ id }, authConfig.secret, {
      expiresIn: authConfig.ttl
    })
  }

  User.associate = models => {
    User.belongsToMany(models.Preference, {
      through: 'users_preferences',
      as: { singular: 'preference', plural: 'preferences' },
      foreingKey: 'user_id'
    })

    User.belongsToMany(models.Meetup, {
      through: 'meetups_subscribes',
      as: { singular: 'meetup', plural: 'meetups' },
      foreingKey: 'user_id'
    })
  }

  return User
}
